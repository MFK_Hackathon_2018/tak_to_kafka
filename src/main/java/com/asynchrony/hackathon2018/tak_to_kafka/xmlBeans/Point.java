package main.java.com.asynchrony.hackathon2018.tak_to_kafka.xmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class Point {

    @Attribute
    String lat;

    @Attribute
    String lon;

    public Point() {
        super();
    }

    public Point(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "Point{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }
}
