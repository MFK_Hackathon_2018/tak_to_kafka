package main.java.com.asynchrony.hackathon2018.tak_to_kafka.xmlBeans;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Event {

    @Element
    private Point point;

    @Attribute
    private String type;

    public Event() {
        super();
    }

    public Event(Point point, String type) {
        this.point = point;
        this.type = type;
    }

    public Point getPoint() {
        return point;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Event{" +
                "point=" + point +
                ", type='" + type + '\'' +
                '}';
    }
}
