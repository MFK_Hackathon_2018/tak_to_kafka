package main.java.com.asynchrony.hackathon2018.tak_to_kafka;

import main.java.com.asynchrony.hackathon2018.tak_to_kafka.xmlBeans.Event;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String... arg) throws IOException {
        Socket socket = new Socket("127.0.0.1", 2114);

        System.err.println("Starting Socket");
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String lineRead = bufferedReader.readLine();
                    System.err.println("LineRead: " + lineRead);

                    Serializer serializer = new Persister();
                    Event read = serializer.read(Event.class, lineRead);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1, TimeUnit.SECONDS);
    }


}